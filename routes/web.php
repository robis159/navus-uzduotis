<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'Web\LandingPageController@index')->name('landing-page');
Route::get('/gallery', 'ControlPanel\controlPanelController@index')->name('gallery');
Route::post('/image-upload', 'Web\FileUploadController@uploadImage')->name('image-upload');
Route::post('/image-edit', 'Web\FileUploadController@editImage')->name('image-edit');
Route::post('/image-delete', 'Web\FileUploadController@deleteImage')->name('image-delete');
