## install
  - composer
  - npm

## bash comands
  - php artisan migrate
  - php artisan user:register

## Bendra informacija

  - Naujai idiegus sistemą reikia įkelti nuotraukas
  - Norint patekti i valdymo pultą reikia atidaryti route /login
  - Naujo vartotojo registravimas
    - Atidaryti CMD projekto direktorijoje
    - Įveskite Komanda: php artisan user:register email password
      - email ir password pakeiskite savo norimais

## Valdymo pultas
  - "Gallery" Rodo visų įkeltų nuotraukų saraša
    - "Upload image" Mygtukas leidžia įkelti nauja nuotrauka bei jai priskirti pavadinimą
    - Užvedus ant nuotraukos matome Edit ir Delete ikonas
      - Edit (piesštukas) leidžia jums koreguoti pasirinkta nuotrauką pakeisti ja pakeisti jos pavadinimą
      - Delete (šiukšliadėžė) leidžia jums ištrinti pasirinktą nuotrauką
  - "Logout" Jus atjungs is valdymo pulto ir nukreips i Pradzios puslapį

## Užduoties atlikimo informacija

  - Trukme: 23h
  - Sudėtingos vietos
    - resources/views/landing_page/facts.blade.php
      - responsive padarymas kad kvadratai gražia butu prikibe prie img sono
    - Valdymo pulto dizaino parinkimas