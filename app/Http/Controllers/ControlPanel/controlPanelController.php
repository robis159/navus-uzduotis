<?php

namespace App\Http\Controllers\ControlPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ImageUpload;

class controlPanelController extends Controller
{
    protected $img;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ImageUpload $img)
    {
        $this->middleware('auth');
        $this->img = $img;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('control_panel.gallery', [
            'imgList' => $this->img->get()
        ]);
    }
}
