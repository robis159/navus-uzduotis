<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ImageUpload;

class FileUploadController extends Controller
{
    private $imageUpload;

    public function __construct(ImageUpload $imageUpload) {
        $this->imageUpload = $imageUpload;
    }

    public function uploadImage(Request $request) {
        \Log::debug('upload');
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time();
        $imageNameWitExtension = $imageName . '.' . $request->image->extension();

        $request->image->move(public_path('images_slider'), $imageNameWitExtension);

        if (!empty($request->imgName)) {
            $imageName = $request->imgName;
        }

        $this->imageUpload->create([
            'name' => $imageName,
            'path' => "images_slider/{$imageNameWitExtension}"
        ]);

        return redirect()->route('gallery');
    }

    public function editImage(Request $request) {
        $selectedImage = $this->imageUpload->find($request->imgId);

        if (!empty($request->image)) {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            unlink(public_path($selectedImage->path));

            $imageName = time();
            $imageNameWitExtension = $imageName . '.' . $request->image->extension();

            $request->image->move(public_path('images_slider'), $imageNameWitExtension);

            if (!empty($request->imgName)) {
                $imageName = $request->imgName;
            }

            $selectedImage->update([
                'name' => $imageName,
                'path' => "images_slider/{$imageNameWitExtension}"
            ]);

        } else {
            $selectedImage->update([
                'name' => $request->imgName
            ]);
        }

        return redirect()->route('gallery');
    }

    public function deleteImage(Request $request) {
        $selectedImage = $this->imageUpload->find($request->imgId);

        unlink(public_path($selectedImage->path));

        $selectedImage->delete();

        return redirect()->route('gallery');
    }
}
