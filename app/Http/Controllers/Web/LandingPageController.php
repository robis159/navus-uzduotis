<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\ImageUpload;

class LandingPageController extends Controller
{
    protected $img;

    public function __construct(ImageUpload $img)
    {
        $this->img = $img;
    }

    public function index() {
        return view('landing_page.index', [
            'imgList' => $this->img->get()
        ]);
    }
}
