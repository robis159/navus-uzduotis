<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

use App\Models\User;

class registerUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:register { email } { password }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register new user for Control panel example: user:register email password';

    protected $user;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        parent::__construct();

        $this->user = $user;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->user->create([
            'email' => $this->argument('email'),
            'password' => Hash::make($this->argument('password'))
        ]);

        dd('User was created!');
    }
}
