<div class="sidebar">
  <div class="sidebar-button">
    <div class="icon icon-gallery"></div>
    <div class="title">
      <a class="" href="{{ route('gallery') }}">
      {{ __('Gallery') }}
      </a>
    </div>
  </div>
  <div class="sidebar-button">
    <div class="icon icon-logout"></div>
    <div class="title">
      {{-- <p>Logout</p> --}}
      <a class="" href="{{ route('logout') }}"
        onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
        {{ __('Logout') }}
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </div>
  </div>
</div>