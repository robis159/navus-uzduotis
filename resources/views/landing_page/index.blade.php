<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>

  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('css/landing_page.css') }}" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
  @include('landing_page.facts')
  @include('landing_page.typography')
  @include('landing_page.screens')
  @include('landing_page.see_it_live')
  @include('landing_page.footer')
  <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  <script src="{{ url::asset('js/landingPage.js')}}"></script>
  <script src="{{ url::asset('js/vanilla-tilt.js')}}"></script>
</body>
</html>