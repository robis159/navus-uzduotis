<div class="typography">
  <p class="title">TIPOGRAFIKA</p>
  <div class="example-cont">
    <div class="text-col">
        <div class="letters">Aa</div>
        <p class="font-name">Avenir Next Cyr Regular, Medium</p>
        <p class="text text-uppercase">absdefghijklmnopqrstuvwx</p>
        <p class="text">absdefghijklmnopqrstuvwx</p>
        <p class="text">1234567890</p>
    </div>
    <div class="vertical-line"></div>
    <div class="horizontal-line"></div>
    <div class="color-col">
      <div class="color">
        <div class="color-293d83"></div>
        <p class="code">#293d83</p>
      </div>
      <div class="color">
        <div class="color-4da4ff"></div>
        <p class="code">#4da4ff</p>
      </div>
    </div>
  </div>

</div>