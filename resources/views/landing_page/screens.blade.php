@if (count($imgList) > 0)
  <div class="screens">
    @foreach ($imgList as $img)
      <div class="img-card">
        <img src="{{ $img->path }}" alt="" srcset="">
      </div>
    @endforeach
  </div>
@else
  <div class="no-images">
    Nuotraukų sąrašas tusčias
    <br>
    Prašome nueiti į valdymo pultą ir pridėti nuotraukas
  </div>
@endif
