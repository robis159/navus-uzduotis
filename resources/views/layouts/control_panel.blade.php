<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  {{-- script --}}
  <script src="{{ asset('js/app.js') }}" defer></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   <!-- Styles -->
   <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
   <link rel="stylesheet" href="{{ URL::asset('css/control_panel.css') }}" />
</head>
<body>
  <div id="app">
    @include('components.header')
    <div class="content">
        @include('components.sidebar')
        @yield('content')
    </div>
  </div>
  @include('control_panel.script')
</body>
</html>