@extends('layouts.control_panel')

@section('content')
  <div id="galleryContent" class="galleryContent">
    <div class="panel-title">
      <p>Gallery</p>
      <div class="action">
        <button id="toggleUpload" class="btn btn-primary">Upload image</button>
      </div>
    </div>
    <div class="panel-content">
      @foreach ($imgList as $img)
      <div class="photo-card">
      <img src="{{ $img->path }}">
      <div class="img-hover">
        <div class="actions">
          <div onClick="showImgEdit({{ $img }})" class="icon icon-edit"></div>
          <div onClick="showImgDelete({{ $img }})" class="icon icon-delete"></div>
        </div>
      </div>
      </div>
      @endforeach
    </div>
    {{-- upload new img --}}
    <div id="selectImageInput" class="select-image">
      <form action="{{ route('image-upload') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="">
            <div class="show-img">
              <img id="blahShow" src="#" alt="your image"/>
            </div>
            <input type="file" name="image" class="file-input" onchange="readURL(this)">
            <input type="text" name="imgName" class="img-name-input" placeholder="Set name for Image">
            <div class="button-row">
                <button type="button" id="cancelUploade" class="btn btn-danger">Cancel</button>
                <button type="submit" class="btn btn-success btn-width">Upload</button>
            </div>
          </div>
      </form>
    </div>
    {{-- edit selected img --}}
    <div id="editSelectedImg" class="select-image">
      <form action="{{ route('image-edit') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="">
          <div class="show-img">
            <img id="blahEdit" src="#" alt="your image"/>
          </div>
          <input id="imgSelector" type="file" name="image" class="file-input" onchange="readURL(this, 'edit')">
          <input id="imgName" type="text" name="imgName" class="img-name-input" placeholder="Set name for Image">
          <input id="imgId" type="hidden" name="imgId">
          <div class="button-row">
              <button type="button" id="cancelEdit" class="btn btn-danger">Cancel</button>
              <button type="submit" class="btn btn-success btn-width">Edit</button>
          </div>
        </div>
      </form>
    </div>
    {{-- Delete selected img --}}
    <div id="deleteSelectedImg" class="select-image delete-card">
      <form action="{{ route('image-delete') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="">
          <div class="show-img">
            <img id="blahDelete" src="#" alt="your image"/>
          </div>
          <p class="delete-question">Delete this image?</p>
          <input id="imgIdDelete" type="hidden" name="imgId">
          <div class="button-row">
              <button type="button" id="cancelDelete" class="btn btn-danger btn-no">No</button>
              <button type="submit" class="btn btn-success btn-width">Yes</button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection