<script>
var toggleSidebar = false
var showImageUploade = false
$(document).ready(function() {
  hideImageUpload()
  hideImageEdit()

  $(window).resize(function(){
    if ($('body').width() > 767) {
      $('.sidebar').css({left: 0})
      toggleSidebar = true
    } else {
      $('.sidebar').css({left: -225})
      toggleSidebar = false
    }
  });

  $('#togleSidebar').on('click', () => {
    toggleSidebar = !toggleSidebar
    if (toggleSidebar) {
      $('.sidebar').css({left: 0})
    } else {
      $('.sidebar').css({left: -225})
    }
  })

  $('.sidebar-button').on('click', () => {
    if ($('body').width() < 767) {
      toggleSidebar = false
      $('.sidebar').css({left: -225})
    }
  })

  $('#toggleUpload').on('click', () => {
    showImageUpload()
  })

  $('#cancelUploade').on('click', () => {
    hideImageUpload()
  })

  $('#cancelEdit').on('click', () => {
    hideImageEdit()
  })

  $('#cancelDelete').on('click', () => {
    hideImageDelete()
  })
})

function showImageUpload() {
  setCardPosition()
  $('#selectImageInput').show()
}

function hideImageUpload() {
  $('#selectImageInput').hide()
}

function showImageEdit() {
  setCardPosition()
  $('#editSelectedImg').show()
}

function hideImageEdit() {
  $('#editSelectedImg').hide()
}

function showImageDelete() {
  setCardPosition()
  $('#deleteSelectedImg').show()
}

function hideImageDelete() {
  $('#deleteSelectedImg').hide()
}

function showImgEdit(img) {
  $('#blahEdit').attr('src', img.path);
  $('#imgName').attr('value', img.name);
  $('#imgId').attr('value', img.id);
  showImageEdit()
}

function showImgDelete(img) {
  $('#blahDelete').attr('src', img.path);
  $('#imgIdDelete').attr('value', img.id);
  showImageDelete()
}

function readURL(input, action = 'view') {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      if (action === 'edit') {
        $('#blahEdit').attr('src', e.target.result);
      } else {
        $('#blahShow').attr('src', e.target.result);
      }
    };

    reader.readAsDataURL(input.files[0]);
  }
}

function setCardPosition() {
  let fromTop = 45
  let schrolPosition = $('html').scrollTop()
  if (schrolPosition > 40) {
    fromTop = schrolPosition
  }
  $('.select-image').css({top: fromTop})
}
</script>