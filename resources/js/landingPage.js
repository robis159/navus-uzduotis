var scale = 1

$(document).ready(function(){
  setBlockPosition()

  $(window).resize(function(){
    setBlockPosition()
  });
});

function setBlockPosition() {
  let imgWidth = $("img").width();
  let imgHeight = $("img").height();

  scaleBlock(imgWidth)
  position(imgWidth, imgHeight)

  let margin = 140 * (scale * 1.5)

  $("img").css({'margin-bottom': margin})
}

function position(imgWidth, imgHeight) {
  let blockHeight = $('.block-container').height()
  let newBlockH = blockHeight - (blockHeight * scale)
  let fromTop = imgHeight - (blockHeight * scale) + 120 - (newBlockH / 1.5)
  let fromLeft = imgWidth - 120 - (newBlockH / 3)
  $('.block-container').css({top: fromTop, left: fromLeft})
}

function scaleBlock(imgWidth) {
  let scaleBlock = 'scale(1)'
  if (imgWidth < 1000) {
    scale = imgWidth * 100 / 1000 / 100
    scaleBlock = `scale(${scale})`
  } else {
    scale = 1
  }
  $('.block-container').css({transform: scaleBlock })
}

$('.screens').slick({
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
  autoplay: true,
  autoplaySpeed: 2000,
});