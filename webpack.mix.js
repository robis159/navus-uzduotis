const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/landingPage.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/Landing_page/landing_page.scss', 'public/css')
   .sass('resources/sass/Control_panel/control_panel.scss', 'public/css');


mix.browserSync({
   proxy: 'navus-task.local'
})